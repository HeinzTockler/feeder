///адреса хранения данных о поливе
#define sh_clk_adr 0
#define lon_adr (sh_clk_adr+sizeof(int))
#define lperiod_adr (lon_adr+sizeof(byte))
#define dperiod_adr (lperiod_adr+sizeof(byte))
#define mode_l_adr (dperiod_adr+sizeof(byte))
#define mode_f_adr (mode_l_adr+sizeof(byte))

//метод для проверки параметров записанных в пзу
//если то что лежит в еепром отлшичается от дефолта то обновляем.
void checkEEPROM() {
  int Sh_clk = 0;
  byte l_on = 0;
  byte l_per = 0;
  byte d_per = 0;
  byte m_l = 0;
  byte m_f = 0;
  /*адрес   что лежит
    0      Shnek_clk кол во оборотов
    1      feed_count периоды кормления
    2      LightOn когда не кормим низ
    3      LightOff когда не кормим верх
  */
  EEPROM.get(sh_clk_adr, Sh_clk);
  EEPROM.get(lon_adr, l_on);
  EEPROM.get(lperiod_adr, l_per);
  EEPROM.get(dperiod_adr, d_per);
  EEPROM.get(mode_l_adr, m_l);
  EEPROM.get(mode_f_adr, m_f);
/*

  if (debug)Serial.print("Sh_clk= ");
  if (debug)Serial.println(Sh_clk);
  if (debug)Serial.print("l_on= ");
  if (debug)Serial.println(l_on);
  if (debug)Serial.print("l_per= ");
  if (debug)Serial.println(l_per);
  if (debug)Serial.print("d_per= ");
  if (debug)Serial.println(d_per);
  if (debug)Serial.print("m_l= ");
  if (debug)Serial.println(m_l);
  if (debug)Serial.print("m_f= ");
  if (debug)Serial.println(m_f);
*/
  //кусок тупого кода, не хотелось с массивом заморачиваться
  if (Sh_clk != Shnek_clk && Sh_clk != 0 && Sh_clk != 255) Shnek_clk = Sh_clk;
  if (l_on != LightOn && l_on != 0 && l_on != 255) LightOn = l_on;
  if (l_per != LightPeriod && l_per != 0 && l_per != 255) LightPeriod = l_per;
  if (d_per != DayFeed && d_per != 0 && d_per != 255) DayFeed = d_per;
  if (m_l != mode_light && m_l != 0 && m_l != 255) mode_light = m_l;
  if (m_f != mode_feed && m_f != 0 && m_f != 255) mode_feed = m_f;
  EEPROM.end();
}

//метод для обновления параметра в ПЗУ
bool writeEEPROM(byte val, byte adr) {
  byte param = 0;
  param = EEPROM.read(adr);
  if (param != val) {
    EEPROM.write(adr, val);
   // if (debug) Serial.println("write");
    return true;
  } else return false;
}

bool writeEEPROM_int(int val, byte adr) {
  int param = 0;
 // if (debug) Serial.print("adr= ");
 // if (debug) Serial.print(adr);
 // if (debug) Serial.println(" ");
  // EEPROM.begin(25);
  EEPROM.get(adr, param);
 // if (debug) Serial.print("param= ");
 // if (debug) Serial.print(param);
 // if (debug) Serial.println(" ");
  if (param != val) {
    EEPROM.put(adr, val);
  //  if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
     // Serial.println(param);
    }
    // EEPROM.commit();
    return true;
  } else return false;
  // EEPROM.end();
}
