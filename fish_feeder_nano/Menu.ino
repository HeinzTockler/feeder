
class MyRenderer : public MenuComponentRenderer {
  public:
    void render(Menu const& menu) const {
       display.clearDisplay();
      menu.render(*this);
      menu.get_current_component()->render(*this);
      display.display();
    }

    void render_menu_item(MenuItem const& menu_item) const {
        display.setTextSize(2);
        display.setTextColor(WHITE);
        display.setCursor(0, 16);
        display.print(menu_item.get_name());
        display.drawFastHLine(5, 9, 118, WHITE);
        display.setCursor(40, 16);
    }

    void render_back_menu_item(BackMenuItem const& menu_item) const {
      display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(0, 16);
      display.print(menu_item.get_name());
    }

    void render_numeric_menu_item(NumericMenuItem const& menu_item) const {
        display.setTextSize(1);
        display.setTextColor(WHITE);
        display.setCursor(35, 0);
        display.print(menu_item.get_name());
        // if (debug)Serial.println(menu_item.get_name());
        //Serial.println(menu_item.get_formatted_value());
        display.drawFastHLine(5, 9, 118, WHITE);
        display.drawFastHLine(5, 10, 118, WHITE);

        display.setTextSize(2);
        display.setTextColor(WHITE);
        display.setCursor(40, 16);

        if (menu_item.get_name() == "Light Mode") {
        display.setTextSize(1);
        if (menu_item.get_value() == 0) {
          display.print("Normal");
        } else if (menu_item.get_value() == 1) {
          display.print("Disable");
        }
        } else  if (menu_item.get_name() == "Feed Mode") {
        display.setTextSize(1);
        if (menu_item.get_value() == 0) {
          display.print("Every1feed");
        } else if (menu_item.get_value() == 1) {
          display.print("Across1feed");
        } else if (menu_item.get_value() == 2) {
          display.print("Disable");
        } else if (menu_item.get_value() == 3) {
          display.print("Every2feed");
        } else if (menu_item.get_value() == 4) {
          display.print("Across2feed");
        }
        } else display.print(menu_item.get_value());
    }

    void render_menu(Menu const& menu) const {
 display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(0, 0);
      display.print(menu.get_name());
    }
};
MyRenderer my_renderer;

MenuSystem ms(my_renderer);
NumericMenuItem nmfeed    ("Shnek_clk", &food_sel, Shnek_clk, 0, MOTOR_STEPS, 50, nullptr);
NumericMenuItem nmlightOn ("LightOn", &light_sel, LightOn, 7, 15, 1, nullptr);
NumericMenuItem nmlperiod ("LightPeriod", &lperiod_sel, LightPeriod, 4, 12, 1, nullptr);
NumericMenuItem nmdayperiod   ("DayFeed", &dayperiod_sel, DayFeed, 1, 7, 1, nullptr);
NumericMenuItem mode_light_m   ("Light Mode", &mode_light_selected, mode_light, 0, 1, 1, nullptr);
NumericMenuItem mode_feed_m   ("Feed Mode", &mode_feed_selected, mode_feed, 0, 4, 1, nullptr);

void food_sel(MenuComponent * p_menu_component) {
  Shnek_clk = nmfeed.get_value();
}

void light_sel(MenuComponent * p_menu_component) {
  LightOn = nmlightOn.get_value();
}

void lperiod_sel(MenuComponent * p_menu_component) {
  LightPeriod = nmlperiod.get_value();
}

void dayperiod_sel(MenuComponent * p_menu_component) {
  DayFeed = nmdayperiod.get_value();
}

void mode_light_selected(MenuComponent * p_menu_component) {
  mode_light = mode_light_m.get_value();
}

void mode_feed_selected(MenuComponent * p_menu_component) {
  mode_feed = mode_feed_m.get_value();
}

void menuInit() {
  //ms.get_root_menu().add_item(&nmperiod);
  ms.get_root_menu().add_item(&nmfeed);
  ms.get_root_menu().add_item(&nmlightOn);
  ms.get_root_menu().add_item(&nmlperiod);
  ms.get_root_menu().add_item(&nmdayperiod);
  ms.get_root_menu().add_item(&mode_light_m);
  ms.get_root_menu().add_item(&mode_feed_m);
  ms.display();
}

void buton_handler() {
  bool s = digitalRead(but_s);
  bool r = digitalRead(but_r);
  bool l = digitalRead(but_l);
  //if (debug)Serial.println("handler");
  if (l && r) {
    menu = false;//выход из меню
    lsd_exit();//рисуем EXIT
    display.clearDisplay();//
    lsd_preset();
    curTime[0] = 0;
    curTime[1] = 0;
    getTime();
    lsd_count();
    lsd_mode();
    //lsd_nexttime();
    writeEEPROM_int(Shnek_clk, sh_clk_adr);
    writeEEPROM(LightOn, lon_adr);
    writeEEPROM(LightPeriod, lperiod_adr);
    writeEEPROM(DayFeed, dperiod_adr);
    writeEEPROM(mode_light, mode_l_adr);
    writeEEPROM(mode_feed, mode_f_adr);
  } else {
    if (l) {
      ms.prev();
      ms.display();
    }
    if (r) {
      ms.next();
      ms.display();
    }
    if (s) {
      ms.select();
      ms.display();
    }
  }
  delay(200);
}
