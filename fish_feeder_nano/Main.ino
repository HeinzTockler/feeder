
void motor_init() {
  //if (debug) Serial.println("motor init");
  bunker.setMaxSpeed(500);
  bunker.setSpeed(500);
  bunker.setAcceleration(500);

  shnek.setMaxSpeed(500);
  shnek.setSpeed(500);
  shnek.setAcceleration(500);

  //  holder.setMaxSpeed(500);
  //  holder.setSpeed(500);
  //  holder.setAcceleration(500);
}

void Main() {
  /*mode_light
    режим света
    0 обычный светим всеми 3 светльниками весь период
    1 светим только первым
    2 светим пол периода первым, потом пол периода 2
    3 ничем не светим
  */
  /*mode_feed
     режим кормления
     0 кормим каждый день учитывая DayFeed 1 раз в день кормим
     1 кормим через день, учитывая DayFeed 1 раз в день кормим
     2 совсем не кормим
     3 кормим каждый день учитывая DayFeed 2 раз в день кормим небольшими порциями
     4 кормим через день учитывая DayFeed 2 раз в день кормим небольшими порциями
  */
  // if (debug)Serial.println("main");
  getTime();
  // if (debug)Serial.println("before");
  //в зависимости от режима включаем необходимые выхода
  if (curTime[0] >= LightOn && curTime[0] < ( LightOn + LightPeriod) && !light)
    if (curTime[1] >= 2) {
      // if (debug)Serial.println("inside");
      if (mode_light == 0) {
        // if (debug)Serial.println("all light");//++
        LightCtrl(true);
        light = true;
      } else if (mode_light == 1) {
        //if (debug)Serial.println("light off");//++
        LightCtrl(false);
        light = true;
      }
    }

  // if (debug)Serial.println(DayFeed);
  //if (debug)Serial.println(weekday());
  //если настало время кормить(одновременно со светом)
  if (curTime[0] == LightOn)
    if (curTime[1] == 3)

      if (weekday() <= DayFeed) {
        // if (debug)Serial.println("feedtime");

        if (mode_feed == 0) {
          feed(Shnek_clk, true);
          // if (debug)Serial.println("feed 1 times every");//++
        } else if (mode_feed == 2) {
          // if (debug)Serial.println("feed disable");//++
          ///ничего не делаем
        } else if (mode_feed == 3) {
          //  if (debug)Serial.println("feed 2 times every");//++
          feed(Shnek_clk / 2, true);
        }
      }

  //если настало время и режим кормления 2 раза в день
  if (curTime[0] == LightOn + LightPeriod && mode_feed == 3)
    if (curTime[1] == 2)
      if (weekday() <= DayFeed) {
        {
          //if (debug)Serial.println("feed 2 times every second");//++
          feed(Shnek_clk / 2, true);
        }
      }

  //если режим кормления через день
  if (curTime[0] == LightOn)
    if (curTime[1] == 2)
      if (weekday() <= DayFeed)
        if ((weekday() % 2) > 0 && (mode_feed == 1 || mode_feed == 4)) {
          if (mode_feed == 1) {
            feed(Shnek_clk, true);
            // if (debug)Serial.println("feed 1 times across");//++
          } else if (mode_feed == 4) {
            feed(Shnek_clk / 2, true);
            //  if (debug)Serial.println("feed 2 times across");//++
          }
        }

  //если настало время и режим кормления 2 раза через день
  if (curTime[0] == LightOn + LightPeriod && mode_feed == 4)
    if (curTime[1] == 2)
      if (weekday() <= DayFeed)
        if ((weekday() % 2 ) > 0 && mode_feed == 4) {
          feed(Shnek_clk / 2, true);
          // if (debug)Serial.println("feed 2 times across second");//+
        }


  //в конце периода света выключаем все
  if (curTime[0] == (LightOn + LightPeriod))
    if (curTime[1] >= 2) {
      LightCtrl(false);
      light = false;
    }

  //в 20.00 включаем подсветку
  if (curTime[0] >=  (LightOn + LightPeriod) && !seclight && curTime[0] < 23)
    if (curTime[1] >= 2) {
      SecLightCtrl(true);
      seclight = true;
    }

  if (curTime[0] >= 23 && seclight)
    if (curTime[1] >= 30) {
      SecLightCtrl(false);
      seclight = false;
    }
}

//метод для получения текущего времени
void getTime() {
  //  if (debug) Serial.println("get");
  curTime[2] = curTime[0];
  curTime[0] = hour();
  curTime[3] = curTime[1];
  curTime[1] = minute();
  // digitalClockDisplay();
  if (!sleepmode) lsd_time(); else {
    display.clearDisplay();
    display.display();
  }
}

//Метод управления мотором. кол-во оборотов и направление
void feed(int steps, bool del) {
  // holder.setCurrentPosition(0);
  bunker.setCurrentPosition(0);
  shnek.setCurrentPosition(0);
  //дозируем корм в шнек
  bunker.runToNewPosition(steps / 2);
  bunker.disableOutputs();
  delay(200);
  shnek.runToNewPosition(1000);
  shnek.disableOutputs();
  delay(200);
  bunker.setCurrentPosition(0);
  shnek.setCurrentPosition(0);
  bunker.runToNewPosition(steps / 2);
  bunker.disableOutputs();
  delay(200);
  //проталкиваем корм
  shnek.runToNewPosition(20000);
  shnek.disableOutputs();
  if (del)
    delay(60000);
}

//Включаем свет по команде
void LightCtrl(bool light) {
  digitalWrite(l1, !light);
}

//Включаем свет по команде
void SecLightCtrl(bool light) {
  digitalWrite(l2, !light);
}
