
//выводим скелет записей на экран
void lsd_preset() {
  //добавить секцию с следующим временем кормления
  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(29, 0);
  display.println(":");

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(100, 24);
  display.println("clk");

  display.display();
}

//вывод на экран времени
void lsd_time() {
  // if (debug) Serial.println("lsd_time");
  if (curTime[0] != curTime[2]) {
    display.fillRect(0, 0, 30, 24, BLACK);
    display.setTextSize(3);
    display.setTextColor(WHITE);
    // if (curTime[0] < 10 || curTime[0] == 0) {
    if (!curTime[0] >= 10) {
      display.setCursor(16, 0);
    } else
      display.setCursor(0, 0);
    display.println(curTime[0]);
  } //else if (debug)   Serial.println("Time equals");

  if (curTime[1] != curTime[3]) {
    display.fillRect(40, 0, 40, 24, BLACK);
    display.setTextSize(3);
    display.setTextColor(WHITE);
    // if (curTime[1] < 10 || curTime[1] == 0) {
    if (curTime[1] < 10) {
      display.setCursor(40, 0);
      display.println("0");
      display.setCursor(56, 0);
    } else
      display.setCursor(40, 0);
    display.println(curTime[1]);
  }// else if (debug)   Serial.println("Time equals");
  display.display();
}

//вывод на экран периода кормления
void lsd_mode() {
  //String feedMode[5] = {"Every1feed", "Across1feed", "Disable", "Every2feed", "Across2feed"};
  //String lightMode[4] = {"Normal", "OnlyFirst", "First/2 Second/2", "Disable"};
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 24);

  if (mode_feed == 0) {
    display.println("Every1feed");
  } else if (mode_feed == 1) {
    display.println("Across1feed");
  } else if (mode_feed == 2) {
    display.println("Disable");
  } else if (mode_feed == 3) {
    display.println("Every2feed");
  } else if (mode_feed == 4) {
    display.println("Across2feed");
  }

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(80, 0);

  if (mode_light == 0) {
    display.println("Normal");
  }  else if (mode_light == 1) {
    display.println("Disable");
  }
  // display.println("light");
}

//вывод на экран количества кормешки
void lsd_count() {
  display.fillRect(80, 24, 10, 8, BLACK);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(70, 24);
  display.println(Shnek_clk);
  display.display();
}

//метод для вывода приветсвия
void lcd_init() {
  display.clearDisplay();
  if (!debug) {
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(50, 0);
    display.println("FISH");
    display.display();

    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(30, 16);
    display.println("Feeder");
    display.display();

    delay(3000);
    display.clearDisplay();
    delay(1000);

    display.setTextSize(3);
    display.setTextColor(WHITE);
    display.setCursor(15, 6);
    display.println("by ET");
    display.display();
    delay(2000);
    display.clearDisplay();
  }
}

//Метод который отрисовывает выход из меню
void lsd_exit() {
  display.clearDisplay();
  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(30, 6);
  display.println("EXIT");
  display.display();
  delay(2000);
}

//Метод который отрисовывает вход в меню
void lsd_menu() {
  display.clearDisplay();
  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(30, 6);
  display.println("MENU");
  display.display();
  delay(2000);
}

void lsd_update() {
  display.clearDisplay();
  lsd_count();
  lsd_preset();
  lsd_mode();
}
