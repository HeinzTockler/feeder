
/*Допилить:
  режимы работы освещения и кормления
  перебить дисплей под большие буквы
  сделать метод от отравления дисплея
*/
//панелька
//#include <SPI.h>
//#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
//#include "ASOLED.h"

//шаговик
#include <AccelStepper.h>
//время
#include <TimeLib.h>
#include <DS3232RTC.h>

//сервисная шляпа
#include <Wire.h>
#include <EEPROM.h>
//менюшка
#include <MenuSystem.h>

//Кнопки
#define but_s  11
#define but_l  13
#define but_r  12
//ноги шаговика
#define MOTOR_STEPS 4000
#define OLED_RESET 4
// Определение пинов для управления двигателем
#define bunk_pin1  2
#define bunk_pin2  3
#define bunk_pin3  4
#define bunk_pin4  5

#define shnek_pin1  A6
#define shnek_pin2  8
#define shnek_pin3  9
#define shnek_pin4  10

//#define hold_pin1 14
///#define hold_pin2 15
//#define hold_pin3 16
//#define hold_pin4 17

//свет управление
#define l1 6
//подсветка
#define l2 7

bool debug = true;
//все для прерываний
unsigned long currentT = 0;
unsigned long prevT = 0;
unsigned long butPressT = 0;

#define period  3
#define menudelay  3
//#define sleepdelay 30//время перехода в ждущий режим

//основные переменные
int Shnek_clk = 256; //кол-во оборотов шнека
byte LightOn = 7; //время в которое включаем свет
byte LightPeriod = 10; //период, сколько светим
byte DayFeed = 6; //сколько дней в неделю кормим
byte mode_light = 0;
byte count = 0;
/*
   режим света
   0 обычный светим всеми 3 светльниками весь период
   1 светим только первым
   2 светим под периода первым, потом пол периода 2
   3 ничем не светим
*/
byte mode_feed = 0;
/*
   режим кормления
   0 кормим каждый день учитывая DayFeed 1 раз в день кормим
   1 кормим через день, учитывая DayFeed 1 раз в день кормим
   2 совсем не кормим
   3 кормим каждый день учитывая DayFeed 2 раз в день кормим небольшими порциями
   4 кормим через день учитывая DayFeed 2 раз в день кормим небольшими порциями
*/
///0 часы текущие 1 - минуты текущие 2 -часы предидущие 3 минуты предыдущие
byte curTime[4] = {
  0, 0, 0, 0
};
//bool sleepactive = false;//флаг перехода в ждущий режим
bool sleepmode = false; //флаг перехода в ждущий режим
bool butLong = false; // флаг длительного нажатия
bool menu = false; // флаг входа в меню
bool light = false; // флаг входа в меню
bool seclight = false; // флаг входа в меню

//Stepper_28BYJ_48 stepper(mp1, mp2, mp3, mp4);
AccelStepper bunker(4, bunk_pin1, bunk_pin3, bunk_pin2, bunk_pin4);
AccelStepper shnek(4, shnek_pin1, shnek_pin3, shnek_pin2, shnek_pin4);
//AccelStepper holder(4, hold_pin1, hold_pin3, hold_pin2, hold_pin4);
Adafruit_SSD1306 display(OLED_RESET);

void setup() {

  Wire.begin();
  Serial.begin(115200);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  pinMode(but_l, INPUT);
  pinMode(but_r, INPUT);
  pinMode(but_s, INPUT);

  pinMode(l1, OUTPUT);
  pinMode(l2, OUTPUT);

  LightCtrl(false);
  SecLightCtrl(false);

  setSyncProvider(RTC.get);   // the function to get the time from the RTC
  setSyncInterval(3600000);

  motor_init();
  lcd_init();
  getTime();

  checkEEPROM();//смотрим че там в ПЗУ, если что обновляем
  lsd_count();
  lsd_mode();
  lsd_preset();

  currentT = millis();
  prevT = currentT;
  // Serial.println("START");
}

void loop() {
  currentT = millis();

  if (!menu) {
    if (digitalRead(but_s)) {
      if (!butLong) {
        butPressT = currentT;
        butLong = true;
      } else if (currentT - butPressT > (menudelay * 1000) && butPressT != 0) {
        butPressT = 0;
        lsd_menu();
        menuInit();
        menu = true;
      }
      count = 0;
      sleepmode = false;
      lsd_update();
    } else butLong = false;

    if (currentT >= prevT + period * 1000) {
      prevT = currentT;
      Main();
      if (count < 100 && !sleepmode) count++;
      else {
        count = 0;
        sleepmode = true;
      }
    } else if (prevT > currentT) {
      prevT = currentT;
    }

    //ручное кормление
    if (digitalRead(but_s) && digitalRead(but_r) && digitalRead(but_l)) {
      // if (debug) Serial.println("manualfeed");
      feed(Shnek_clk, false);
      delay(1000);
    }
  } else {
    buton_handler() ;
  }
  //set_time();
}

/*
void set_time() {
  //2020,06,18,10,01,55
  // check for input to set the RTC, minimum length is 12, i.e. yy,m,d,h,m,s
  if (Serial.available() >= 12) {
    static time_t tLast;
    time_t t;
    tmElements_t tm;
    // note that the tmElements_t Year member is an offset from 1970,
    // but the RTC wants the last two digits of the calendar year.
    // use the convenience macros from the Time Library to do the conversions.
    int y = Serial.parseInt();
    if (y >= 100 && y < 1000) {}
    //Serial << F("Error: Year must be two digits or four digits!") << endl;
    else {
      if (y >= 1000)
        tm.Year = CalendarYrToTm(y);
      else    // (y < 100)
        tm.Year = y2kYearToTm(y);
      tm.Month = Serial.parseInt();
      tm.Day = Serial.parseInt();
      tm.Hour = Serial.parseInt();
      tm.Minute = Serial.parseInt();
      tm.Second = Serial.parseInt();
      t = makeTime(tm);
      RTC.set(t);        // use the time_t value to ensure correct weekday is set
      setTime(t);
      //Serial << F("RTC set to: ");
//      digitalClockDisplay();
      // Serial << endl;
      // dump any extraneous input
      while (Serial.available() > 0) Serial.read();
    }
  }
}
*/
