/*Допилить:
  выключение дисплея по времени
*/
//панелька
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h> 
//шаговик
#include <Arduino.h>
#include "A4988.h"
//время
#include <TimeLib.h>
#include <DS1307RTC.h>
//сервисная шляпа
#include <Wire.h>
#include <EEPROM.h>
//менюшка
#include <MenuSystem.h>

//параметры для драйверы
#define MOTOR_STEPS 200
#define RPM 20
#define MICROSTEPS 8

//пины для драйвера
#define DIR 2
#define STEP 3
#define ENABLE 10

//Панелька
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

//структура для модуля времени
tmElements_t timemod;

//Кнопки
byte but_s = 11;
byte but_l = 13;
byte but_r = 12;

A4988 stepper(MOTOR_STEPS, DIR, STEP, ENABLE, true, true, false);

bool debug = true;
//все для прерываний
unsigned long currentT = 0;
unsigned long prevT = 0;
unsigned long butPressT = 0;
unsigned long sleepT = 0;
const byte period = 30;
const byte menudelay = 3;
const byte sleepdelay = 30;//время перехода в ждущий режим
//основные переменные
byte feedcount = 2; //кол-во оборотов шнека
byte timeperiod = 16; //период кормления котана в часах
byte lowlimit = 7; //время раньше которого не кормим
byte hightlimit = 23; //время позже которого не кормим
///0 часы текущие 1 - минуты текущие 2 -часы предидущие 3 минуты предидыщие
byte curTime[4] = {
  0, 0, 0, 0
};
byte nextFeedTime[2] = {
  0, 0
};
bool sleepactive = false;//флаг перехода в ждущий режим
bool sleepmode = false; //флаг перехода в ждущий режим
bool feedCalc = true; //флаг того что пора пересчитать время кормления
bool butLong = false; // флаг длительного нажатия
bool menu = false; // флаг входа в меню


void setup() {

  Wire.begin();
  Serial.begin(115200);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  stepper.begin(RPM, MICROSTEPS);
  stepper.disable();
  stepper.setSpeedProfile(stepper.CONSTANT_SPEED);

  pinMode(but_l, INPUT);
  pinMode(but_r, INPUT);
  pinMode(but_s, INPUT);

  Serial.println("START");

  currentT = millis();
  prevT = currentT;

  feedCalc = true; //только запустили надо писчитать когда кормить

  lcd_init();
  getTime();
  checkEEPROM();//смотрим че там в ПЗУ, если что обновляем
  lsd_preset();
}

//выводим скелет записей на экран
void lsd_preset() {
  //добавить секцию с следующим временем кормления
  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(29, 0);
  display.println(":");

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 24);
  display.println("Period=");

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(90, 24);
  display.println("rotate");

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(80, 0);
  display.println("next");

  display.display();
}

//вывод на экран времени
void lsd_time() {

  if (curTime[0] != curTime[2]) {
    display.fillRect(0, 0, 29, 24, BLACK);
    display.setTextSize(3);
    display.setTextColor(WHITE);
    // if (curTime[0] < 10 || curTime[0] == 0) {
    if (!curTime[0] >= 10) {
      display.setCursor(16, 0);
    } else
      display.setCursor(0, 0);
    display.println(curTime[0]);
  } else if (debug)   Serial.println("Time equals");

  if (curTime[1] != curTime[3]) {
    display.fillRect(40, 0, 40, 24, BLACK);
    display.setTextSize(3);
    display.setTextColor(WHITE);
    // if (curTime[1] < 10 || curTime[1] == 0) {
    if (!curTime[1] >= 10) {
      display.setCursor(40, 0);
      display.println("0");
      display.setCursor(56, 0);
    } else
      display.setCursor(40, 0);
    display.println(curTime[1]);
  } else if (debug)   Serial.println("Time equals");
  display.display();
}

//вывод на экран периода кормления
void lsd_period() {
  // display.fillRect(x, y, w, h, WHITE);
  display.fillRect(42, 24, 20, 8, BLACK);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(42, 24);
  display.println(timeperiod);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  if (timeperiod < 10)
    display.setCursor(50, 24);
  else
    display.setCursor(55, 24);
  display.println("h");
  display.display();
}

//вывод на экран количества кормешки
void lsd_count() {
  display.fillRect(80, 24, 10, 8, BLACK);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(80, 24);
  display.println(feedcount);
  display.display();
}

//вывод на экран время следующей кормешки
void lsd_nexttime() {
  display.fillRect(80, 8, 48, 8, BLACK);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(80, 8);

  if (nextFeedTime[0] < 10) {
    display.println("0");
    display.setCursor(86, 8);
  }
  display.println( nextFeedTime[0]);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(90, 8);
  display.println(":");

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(94, 8);

  if (nextFeedTime[1] < 10) {
    display.println("0");
    display.setCursor(100, 8);
  }
  display.println( nextFeedTime[1]);
  display.display();
}

//метод для вывода приветсвия
void lcd_init() {
  display.clearDisplay();
  if (!debug) {
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(50, 0);
    display.println("CAT");
    display.display();

    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(30, 16);
    display.println("Feeder");
    display.display();

    delay(3000);
    display.clearDisplay();
    delay(1000);

    display.setTextSize(3);
    display.setTextColor(WHITE);
    display.setCursor(15, 6);
    display.println("by ET");
    display.display();
    delay(2000);
    display.clearDisplay();
  }
}

//Метод который отрисовывает выход из меню
void lsd_exit() {
  display.clearDisplay();
  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(30, 6);
  display.println("EXIT");
  display.display();
  delay(2000);
}

//Метод который отрисовывает вход в меню
void lsd_menu() {
  display.clearDisplay();
  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(30, 6);
  display.println("MENU");
  display.display();
  delay(2000);
}

void loop() {
  currentT = millis();

  if (!menu) {
    if (digitalRead(but_s)) {
      if (!butLong) {
        butPressT = currentT;
        butLong = true;
        if (debug)Serial.println("setup");
      } else if (currentT - butPressT > (menudelay * 1000)&butPressT != 0) {
        if (debug)Serial.println("long");
        // butLong = false;
        butPressT = 0;
        lsd_menu();
        menuInit();
        menu = true;
      }
    } else butLong = false;

    if (currentT >= prevT + period * 1000) {
      prevT = currentT;
      Main();
    } else if (prevT > currentT) {
      prevT = currentT;
      Serial.println("Override!!!!");
    }

    /*
        if ( !sleepactive) {
          if (!sleepmode) {
            if (currentT >= (sleepT + sleepdelay * 10000)) {
              if (debug)Serial.println("sleeped");
              sleepactive = true;
            }
          } else sleepT = currentT;

          if (sleepmode) sleepmode = false;
        }

        //переход в ждущий режим
        if ((digitalRead(but_s) || digitalRead(but_r) || digitalRead(but_l)) & !sleepmode) {
          if (debug)Serial.println("but pressed");
          sleepmode = true;
          sleepactive = false;
        }
    */
    //ручное кормление
    if (digitalRead(but_s)&digitalRead(but_r)&digitalRead(but_l)) {
      if (debug) Serial.println("manualfeed");
      feedCat(1, 1);
      //getTime();
      // nextFeedTime[0] = curTime[0] ;
      //nextFeedTime[1] = curTime[1];
      //feedCalc = true;
    }
  } else {
    buton_handler() ;
  }

  /*
    if (digitalRead(but_s)) {
    Serial.println("setup");
    }
    if (digitalRead(but_l)) {
    Serial.println("left");
    }
    if (digitalRead(but_r)) {
    Serial.println("fight");
    }
  */
}

void Main() {
  if (debug)Serial.println("main");
  getTime();
  if (feedCalc)feedTimeCalc();
  else {
    if (curTime[0] == nextFeedTime[0])
      if (curTime[1] == nextFeedTime[1]) {
        if (debug)Serial.println("feedtime");
        feedCat(feedcount, 1);
      }
  }
}

//метод для получения текущего времени
void getTime() {
  if (RTC.read(timemod)) {
    curTime[2] = curTime[0];
    curTime[0] = timemod.Hour;
    if (debug) {
      Serial.print("Cur_H= ");
      Serial.println(curTime[0]);
    }
    curTime[3] = curTime[1];  
    curTime[1] = timemod.Minute;
    if (debug) {
      Serial.print("Cur_M= ");
      Serial.println(curTime[1]);
    }
    lsd_time();
  }
  /*curTime[0] = 12;
    curTime[1] = 55;
    curTime[2] = 9;
    curTime[3] = 23;
    lsd_time();*/
}

//метод для перерасчета следующего времени кормления
void feedTimeCalc() {
  if (debug)Serial.println("feedcalc");
  bool i = true;
  byte k = 1; //кол-во итерации
  //если времени меньше чем нижняя точка некормления то говорим что кормить надо в lowlimit
  if (curTime[0] < lowlimit) {
    nextFeedTime[0] = lowlimit;
    nextFeedTime[1] = 0;
    feedCalc = false;
    // lsd_nexttime();
    if (debug)Serial.println("<");
  }/*
  else if (curTime[0] == lowlimit & nextFeedTime[0] != curTime[0]) {
    nextFeedTime[0] = lowlimit;
    nextFeedTime[1] = curTime[1] + 1;
    feedCalc = false;
    lsd_nexttime();
    if (debug)Serial.println("=");
  }*/
  else {
    if (debug)Serial.println(">");
    while (i) {
      if (k > 200) {
        //случилось гавно и алгоритм пытается зациклится
        i = false;
        nextFeedTime[0] = hightlimit;
        nextFeedTime[1] = 0;
        feedCalc = false;
        i = false;
        //lsd_nexttime();
      }
      //if (debug)Serial.print("iterate");
      //if (debug)Serial.println(k);
      if ((lowlimit + timeperiod * k) > curTime[0]) {
        if ((lowlimit + timeperiod * k) > hightlimit) {
          //если насчитали уже больше чем верхний предел то переносим кормешку на след день
          nextFeedTime[0] = lowlimit;
          nextFeedTime[1] = 0;
          feedCalc = false;
          i = false;
        } else {
          nextFeedTime[0] = lowlimit + timeperiod * k;
          nextFeedTime[1] = 0;
          feedCalc = false;
          i = false;
          if (debug)Serial.println("good");
          //lsd_nexttime();
        }
      }
      k++;
    }
  }
  lsd_nexttime();
  // if (debug)Serial.print("next_H= ");
  // if (debug)Serial.println(nextFeedTime[0]);
  // if (debug)Serial.print("next_M= ");
  // if (debug)Serial.println(nextFeedTime[1]);
}

//Метод управления мотором. кол-во оборотов и направление
void feedCat(byte count, byte dir) {
  int fourth = MOTOR_STEPS / 4;
  Serial.println("feed");
  if (count > 0) {
    stepper.enable();
    for (int i = 1; i <= count; i++) {
      for (int j = 1; j <= 4; j++) {
        stepper.move(2 * fourth * MICROSTEPS * dir);
        delay(100);
        stepper.move(fourth * MICROSTEPS * dir * (-1));
        delay(100);
        /* stepper.move(fourth * MICROSTEPS * dir);
          delay(50);
          stepper.move(fourth * MICROSTEPS * dir);
          //if (debug)Serial.println(stepper.getTimeForMove((long)(3 * fourth * MICROSTEPS * dir)));
          //delay(50);
          //stepper.move(fourth * MICROSTEPS * (dir * -1));
          // if (debug)Serial.println(stepper.getTimeForMove((long)(fourth * MICROSTEPS * (dir * -1))));
          delay(50);
          //stepper.move(3 * fourth * MICROSTEPS * dir);
          stepper.move(fourth * MICROSTEPS * dir);
          delay(50);
          stepper.move(fourth * MICROSTEPS * dir);
          delay(50);
          stepper.move(fourth * MICROSTEPS * dir);
          delay(50);
          //stepper.move(fourth * MICROSTEPS * (dir * -1));
          // delay(50);*/
      }
    }
    stepper.disable();
  }
  feedCalc = true; //говорим что покормили и пора бы пересчитать
}

//метод для проверки параметров записанных в пзу
//если то что лежит в еепром отлшичается от дефолта то обновляем.
void checkEEPROM() {
  byte feed = 0;
  byte timeper = 0;
  byte low = 0;
  byte hight = 0;
  /*адрес   что лежит
    0      feedcount кол во оборотов
    1      timeperiod периоды кормления
    2      lowlimit когда не кормим низ
    3      hightlimit когда не кормим верх
  */
  feed = EEPROM.read(0);
  timeper = EEPROM.read(1);
  low = EEPROM.read(2);
  hight = EEPROM.read(3);
  if (debug)Serial.println(feed);
  if (debug)Serial.println(timeper);
  if (debug)Serial.println(low);
  if (debug)Serial.println(hight);
  //кусок тупого кода, не хотелось с массивом заморачиваться
  if (feed != feedcount & feed != 0 & feed != 255)feedcount = feed;
  if (timeper != timeperiod & timeper != 0 & timeper != 255)timeperiod = timeper;
  if (low != lowlimit & low != 0 & low != 255)lowlimit = low;
  if (hight != hightlimit & hight != 0 & hight != 255)hightlimit = hight;

  lsd_count();
  lsd_period();
}

//метод для обновления параметра в ПЗУ
bool writeEEPROM(byte val, byte adr) {
  byte param = 0;
  param = EEPROM.read(adr);
  if (param != val) {
    EEPROM.write(adr, val);
    if (debug) Serial.println("write");
    return true;
  } else return false;
}

class MyRenderer : public MenuComponentRenderer {
  public:
    void render(Menu const& menu) const {
      display.clearDisplay();
      menu.render(*this);
      menu.get_current_component()->render(*this);
      display.display();
    }

    void render_menu_item(MenuItem const& menu_item) const {
      display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(0, 16);
      display.print(menu_item.get_name());
    }

    void render_back_menu_item(BackMenuItem const& menu_item) const {
      display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(0, 16);
      display.print(menu_item.get_name());
    }

    void render_numeric_menu_item(NumericMenuItem const& menu_item) const {
      display.setTextSize(1);
      display.setTextColor(WHITE);
      display.setCursor(35, 0);
      display.print(menu_item.get_name());
      Serial.println(menu_item.get_formatted_value());
      display.drawFastHLine(5, 9, 118, WHITE);
      display.drawFastHLine(5, 10, 118, WHITE);

      display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(40, 16);
      display.print(menu_item.get_value());
    }

    void render_menu(Menu const& menu) const {
      display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(0, 0);
      display.print(menu.get_name());
    }
};
MyRenderer my_renderer;

MenuSystem ms(my_renderer);
NumericMenuItem nmfeed(" FeedCount", &food_sel, feedcount, 1, 10, 1, nullptr);
NumericMenuItem nmperiod("  Period  ", &period_sel, timeperiod, 1, 23, 1, nullptr);
NumericMenuItem nmlow("    Low  ", &low_sel, lowlimit, 0, 12, 1, nullptr);
NumericMenuItem nmhight("  Hight   ", &hight_sel, hightlimit, 13, 23, 1, nullptr);
//NumericMenuItem nmHour("   Hour   ", &hour_sel, curTime[0], 0, 23, 1, nullptr);
//NumericMenuItem nmMinute("  Minute  ", &min_sel, curTime[1], 0, 59, 1, nullptr);
//! Constructor
//!
//! @param name The name of the menu item.
//! @param select_fn The function to call when this MenuItem is selected.
//! @param value Default value.
//! @param min_value The minimum value.
//! @param max_value The maximum value.
//! @param increment How much the value should be incremented by.
//! @param format_value_fn The custom formatter. If nullptr the String
//!                        float formatter will be used.
/*   NumericMenuItem(const char* name, SelectFnPtr select_fn,
                   float value, float min_value, float max_value,
                   float increment=1.0,
                   FormatValueFnPtr format_value_fn=nullptr);
*/

void period_sel(MenuComponent * p_menu_component) {
  if (debug)Serial.print("Period= ");
  if (debug)Serial.println(nmperiod.get_value());
  timeperiod = nmperiod.get_value();
}

void food_sel(MenuComponent * p_menu_component) {
  if (debug)Serial.print("FeedCount= ");
  if (debug)Serial.println(nmfeed.get_value());
  feedcount = nmfeed.get_value();
}

void low_sel(MenuComponent * p_menu_component) {
  if (debug) Serial.print("Low= ");
  if (debug) Serial.println(nmlow.get_value());
  lowlimit = nmlow.get_value();
}

void hight_sel(MenuComponent * p_menu_component) {
  if (debug) Serial.print("Hight= ");
  if (debug) Serial.println(nmhight.get_value());
  hightlimit = nmhight.get_value();
}

/*
  void hour_sel(MenuComponent* p_menu_component) {
  time_t t;
  tmElements_t tm;
  tm.Hour = (int)nmHour.get_value();
  t = makeTime(tm);
  RTC.set(t);
  setTime(t);
  }

  void min_sel(MenuComponent* p_menu_component) {
  time_t t;
  tmElements_t tm;
  tm.Minute = (int)nmMinute.get_value();
  t = makeTime(tm);
  RTC.set(t);
  setTime(t);
  }*/

void menuInit() {
  ms.get_root_menu().add_item(&nmperiod);
  ms.get_root_menu().add_item(&nmfeed);
  ms.get_root_menu().add_item(&nmlow);
  ms.get_root_menu().add_item(&nmhight);
  //ms.get_root_menu().add_item(&nmHour);
  //ms.get_root_menu().add_item(&nmMinute);
  ms.display();
}

void buton_handler() {
  bool s = digitalRead(but_s);
  bool r = digitalRead(but_r);
  bool l = digitalRead(but_l);
  //if (debug)Serial.println("handler");
  if (l && r) {
    menu = false;//выход из меню
    lsd_exit();//рисуем EXIT
    display.clearDisplay();//
    lsd_preset();
    curTime[0] = 0;
    curTime[1] = 0;
    getTime();
    lsd_count();
    lsd_period();
    lsd_nexttime();
    writeEEPROM(feedcount, 0);

    if (writeEEPROM(timeperiod, 1)) {
      feedCalc = true;
    }
    writeEEPROM(lowlimit, 2);
    writeEEPROM(hightlimit, 3);

    /*адрес   что лежит
      0      feedcount кол во оборотов
      1      timeperiod периоды кормления
      2      lowlimit когда не кормим низ
      3      hightlimit когда не кормим верх
    */
    // } else if (l && r) {
    // ms.back();
    // ms.display();
  } else {
    if (l) {
      ms.prev();
      ms.display();
    }
    if (r) {
      ms.next();
      ms.display();
    }
    if (s) {
      ms.select();
      ms.display();
    }
  }
  delay(200);
}
